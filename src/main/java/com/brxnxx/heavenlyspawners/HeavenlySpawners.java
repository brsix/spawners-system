package com.brxnxx.heavenlyspawners;

import com.brxnxx.heavenlyspawners.commands.SpawnersCommand;
import com.brxnxx.heavenlyspawners.database.ConnectionManager;
import com.brxnxx.heavenlyspawners.database.DataManager;
import com.brxnxx.heavenlyspawners.event.*;
import com.brxnxx.heavenlyspawners.translator.EntityName;
import com.brxnxx.heavenlyspawners.utils.Logger;
import com.brxnxx.heavenlyspawners.utils.gui.InventoryManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class HeavenlySpawners extends JavaPlugin {
    private static HeavenlySpawners instance;
    Logger logger = new Logger("CloudSpawners");
    private ConnectionManager connectionManager;
    private DataManager dataManager;
    private static InventoryManager invManager;
    private Economy economy;

    public static HeavenlySpawners getInstance() {
        return instance;
    }

    public static InventoryManager getInvManager() {
        return invManager;
    }

    @Override
    public void onEnable() {
        instance = this;
        new BukkitRunnable() {
            @Override
            public void run() {
                logger.info("Starting CloudSpawners v" + getDescription().getVersion());
                connectionManager = new ConnectionManager();
                connectionManager.connectSqlite();
                dataManager = new DataManager();

                getCommand("spawners").setExecutor(new SpawnersCommand());

                if (!setupEconomy()) {
                    logger.error("Não foi possivel conectar ao vault, GUI de spawners desativada.");
                }
            }
        }.runTaskLater(instance, 100);
        Bukkit.getPluginManager().registerEvents(new BreakEvent(), instance);
        Bukkit.getPluginManager().registerEvents(new InteractEvent(), instance);
        Bukkit.getPluginManager().registerEvents(new PlaceEvent(), instance);
        Bukkit.getPluginManager().registerEvents(new SpawnerEvent(), instance);
        Bukkit.getPluginManager().registerEvents(new InventoryEvent(), instance);
        Bukkit.getPluginManager().registerEvents(new EntityDamage(), instance);

        invManager = new InventoryManager(instance);
        invManager.init();
    }

    @Override
    public void onDisable() {
        Logger dataManagerLogger = getDataManager().getLogger();
        dataManagerLogger.info("Saving spawners...");
        long l = System.currentTimeMillis();
        int i = dataManager.save(true);
        dataManagerLogger.info("Saved " + i + " spawners in " + (System.currentTimeMillis() - l) + "ms.");
        logger.info("Disabling CloudSpawners v" + getDescription().getVersion());
    }

    public Logger getFLogger() {
        return logger;
    }

    public ConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public String sanitizeName(EntityType type) {
        return EntityName.byEntity(type);
        // String s = type.toString().replaceAll("\\.", " ").toLowerCase();
        //  return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    public Economy getEconomy() {
        return economy;
    }

    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

}
