package com.brxnxx.heavenlyspawners.commands;

import com.brxnxx.heavenlyspawners.inventories.SpawnerInventory;
import com.brxnxx.heavenlyspawners.objects.Spawner;
import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import com.brxnxx.heavenlyspawners.utils.HumanUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.brxnxx.heavenlyspawners.utils.ItemHelper.proccessItem;

public class SpawnersCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (args.length == 0) {
            if (sender instanceof Player) {
                if (HeavenlySpawners.getInstance().getEconomy() != null) {
                    SpawnerInventory.get().getInventory().open((Player) sender);
                    return true;
                }
            }
        } else if (args.length == 1 && args[0].equalsIgnoreCase("top")) {
            sender.sendMessage(ChatColor.DARK_GREEN + "Top de Spawners:" + "\n\n");
            final int[] current = {1};
            HeavenlySpawners.getInstance().getDataManager().getSpawnersTop().forEach((nick, value) -> {
                sender.sendMessage(ChatColor.GREEN + "" + current[0] + "º " + ChatColor.GRAY + nick + " " + ChatColor.WHITE + HumanUtils.humanBalance(value));
                current[0]++;
            });
            return true;
        } else if (!sender.hasPermission("cloudspawners.give")) {
            sender.sendMessage(ChatColor.RED + "Sem permissão.");
            return true;
        }
        if (args.length > 1 && args[0].equalsIgnoreCase("cleanup") && args[1].equalsIgnoreCase("codered")) {
            List<Spawner> spawners = new ArrayList<>(HeavenlySpawners.getInstance().getDataManager().getSpawnerCache().values());
            int total = spawners.size();
            new BukkitRunnable() {
                @Override
                public void run() {
                    for (int i = 0; i < Math.min(spawners.size(), 3); i++) {
                        Spawner spawner = spawners.get(0);
                        if (!spawner.getLocation().getChunk().isLoaded()) {
                            spawner.getLocation().getChunk().load();
                        }
                        if (!spawner.getLocation().getBlock().getType().equals(Material.MOB_SPAWNER)) {
                            spawner.delete();
                            sender.sendMessage(ChatColor.RED + "Removidos " + spawner.getType() + " (" + spawner.getStackedAmount() + ") em " + spawner.getLocation().toStringBlock());
                            spawners.remove(0);
                        } else {
                            sender.sendMessage(ChatColor.GREEN + "Sucesso " + spawner.getType() + " (" + spawner.getStackedAmount() + ") em " + spawner.getLocation().toStringBlock());
                            spawners.remove(0);
                        }
                    }
                    sender.sendMessage(ChatColor.YELLOW + "" + (total - spawners.size()) + "/" + total);
                    if (spawners.size() < 1) cancel();
                }
            }.runTaskTimer(HeavenlySpawners.getInstance(), 0L, 20L);
            return true;
        }
        if (args.length > 2 && args[0].equalsIgnoreCase("give")) {
            try {
                EntityType type = EntityType.valueOf(args[1]);
                Player player = Bukkit.getPlayer(args[2]);
                int amount = 1;
                if (args.length > 3) {
                    try {
                        amount = Integer.parseInt(args[3]);
                    } catch (NumberFormatException e) {
                        sender.sendMessage(ChatColor.RED + args[3] + " não é um numero valido");
                    }
                }
                if (player != null) {
                    ItemStack dropStack = new ItemStack(Material.MOB_SPAWNER, amount);
                    BlockStateMeta meta = (BlockStateMeta) dropStack.getItemMeta();
                    CreatureSpawner spawner = (CreatureSpawner) meta.getBlockState();
                    spawner.setSpawnedType(type);
                    meta.setBlockState(spawner);
                    meta.setDisplayName("§6Gerador de §e" + HeavenlySpawners.getInstance().sanitizeName(type));
                    dropStack.setItemMeta(meta);
                    player.getInventory().addItem(proccessItem(dropStack));
                    player.updateInventory();
                } else {
                    sender.sendMessage(ChatColor.RED + args[2] + " não está online.");
                }
            } catch (IllegalArgumentException e) {
                sender.sendMessage(ChatColor.RED + args[1] + " não é um tipo valido");
            }
        } else if (args.length > 0 && args[0].equalsIgnoreCase("info")) {
            List<Spawner> spawners = new ArrayList<>(HeavenlySpawners.getInstance().getDataManager().getSpawnerCache().values());
            int count = 0;
            int loadedCount = 0;
            HashMap<EntityType, Integer> typeCount = new HashMap<>();
            HashMap<EntityType, Integer> typeCountStacked = new HashMap<>();
            HashMap<String, Integer> userCount = new HashMap<>();
            HashMap<String, Integer> userCountStacked = new HashMap<>();
            for (Spawner spawner : spawners) {
                count++;
                if (spawner.getLocation().getBlock().getChunk().isLoaded()) {
                    loadedCount++;
                }
                typeCountStacked.put(spawner.getType(), typeCountStacked.getOrDefault(spawner.getType(), 0) + spawner.getStackedAmount());
                typeCount.put(spawner.getType(), typeCount.getOrDefault(spawner.getType(), 0) + 1);
                userCountStacked.put(spawner.getOwner(), userCountStacked.getOrDefault(spawner.getOwner(), 0) + spawner.getStackedAmount());
                userCount.put(spawner.getOwner(), userCount.getOrDefault(spawner.getOwner(), 0) + 1);
            }
            sender.sendMessage("Tipos: ");
            typeCount.forEach((entityType, integer) -> {
                sender.sendMessage(ChatColor.RED + "   " + entityType.toString() + ": " + integer + " (" + typeCountStacked.get(entityType) + ")");
            });
            sender.sendMessage(ChatColor.RED + "Players: ");
            userCount.keySet().stream().sorted(Comparator.comparing(userCountStacked::get)).collect(Collectors.toList()).forEach(player -> {
                sender.sendMessage(ChatColor.RED + "   " + player + ": " + userCount.get(player) + " (" + userCountStacked.get(player) + ")");
            });
            sender.sendMessage(ChatColor.RED + "Spawners: " + count + "\nSpawners carregados: " + loadedCount);
        } else {
            sender.sendMessage(ChatColor.RED + "Use /" + label + " info OU /" + label + " give <tipo> <player> [quantidade]");
        }
        return true;
    }
}
