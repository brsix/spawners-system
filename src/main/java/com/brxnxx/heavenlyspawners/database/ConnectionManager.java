package com.brxnxx.heavenlyspawners.database;


import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import com.brxnxx.heavenlyspawners.objects.Spawner;
import com.brxnxx.heavenlyspawners.utils.FileUtils;
import com.brxnxx.heavenlyspawners.utils.Logger;
import com.brxnxx.heavenlyspawners.utils.StringLocation;
import org.bukkit.entity.EntityType;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ConnectionManager {
    private Connection connection;
    private Logger logger;

    public ConnectionManager() {
        logger = new Logger("CloudSpawners", "ConnectionManager");
    }

    public void connectSqlite() {
        logger.info("Connecting to the SQLite database...");
        long ms = System.currentTimeMillis();
        try {
            Class.forName("org.sqlite.JDBC");
            String url = "jdbc:sqlite:" + FileUtils.loadFile(HeavenlySpawners.getInstance().getDataFolder(),"/db/spawners.db").getAbsolutePath();
            connection = DriverManager.getConnection(url);
            logger.info("Connection to SQLite has been established in " + (System.currentTimeMillis() - ms) + "ms.");
            checkTable();
        } catch (SQLException | ClassNotFoundException e) {
            logger.error("Failed to connect to the SQLite database:");
            e.printStackTrace();
        }
    }

    private void checkTable() throws SQLException {
        try (
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM sqlite_master " +
                        "WHERE type='table' AND name='cloudspawners';")
        ) {
            Long l = null;
            if (!resultSet.next()) {
                logger.info("Database table does not exist, creating...");
                l = System.currentTimeMillis();
            }
            statement.execute("CREATE TABLE IF NOT EXISTS `cloudspawners` (\n" +
                    "  `location` VARCHAR(255) NOT NULL,\n" +
                    "  `type` VARCHAR(30) NOT NULL,\n" +
                    "  `owner` VARCHAR(30) NOT NULL,\n" +
                    "  `stackedAmount` int NOT NULL,\n" +
                    "  PRIMARY KEY (`location`)" +
                    ");");
            if (l != null) {
                logger.info("Created table in " + (System.currentTimeMillis() - l) + "ms.");
            }
        }
    }

    public List<Spawner> getAllSpawners() {
        List<Spawner> spawnerList = new ArrayList<>();
        try (
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM cloudspawners;")
        ) {
            while (resultSet.next()) {
                String spawnerLocation = resultSet.getString("location");
                EntityType entityType = EntityType.valueOf(resultSet.getString("type"));
                String owner = resultSet.getString("owner");
                int stackedAmount = resultSet.getInt("stackedAmount");
                spawnerList.add(new Spawner(StringLocation.fromString(spawnerLocation), entityType, owner, stackedAmount));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return spawnerList;
    }

    void saveSpawner(Spawner spawner) {
        try (
                Statement statement = connection.createStatement()
        ) {
            statement.execute("INSERT OR REPLACE INTO cloudspawners VALUES (\"" + spawner.getLocation().toStringBlock() +
                    "\", \"" + spawner.getType().toString() +
                    "\", \"" + spawner.getOwner() +
                    "\", " + spawner.getStackedAmount() + ");");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void deleteSpawner(Spawner spawner) {
        try (
                Statement statement = connection.createStatement()
        ) {
            statement.execute("DELETE FROM cloudspawners WHERE location = '" + spawner.getLocation().toStringBlock() + "'");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
