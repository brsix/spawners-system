package com.brxnxx.heavenlyspawners.database;

import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import com.brxnxx.heavenlyspawners.inventories.SpawnerInventory;
import com.brxnxx.heavenlyspawners.objects.Spawner;
import com.brxnxx.heavenlyspawners.utils.Logger;
import com.brxnxx.heavenlyspawners.utils.StringLocation;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.stream.Collectors;

public class DataManager {
    private Map<String, Spawner> spawnerCache = new HashMap<>();
    private List<Spawner> deletedSpawnerCache = new ArrayList<>();
    private Logger logger = new Logger("CloudSpawners", "DataManager");
    private boolean saving = false;
    private BukkitTask saveTask;
    private LinkedHashMap<String, Double> spawnersTop = new LinkedHashMap <>();

    public DataManager() {
        logger.info("Loading spawners...");
        long l = System.currentTimeMillis();
        List<Spawner> spawnerList = HeavenlySpawners.getInstance().getConnectionManager().getAllSpawners();
        for (Spawner spawner : spawnerList) {
            spawnerCache.put(spawner.getLocation().toStringBlock(), spawner);
        }
        logger.info("Loaded " + spawnerCache.size() + " spawners in " + (System.currentTimeMillis() - l) + "ms.");

        calculateTop();
        new BukkitRunnable() {
            @Override
            public void run() {
                long ms = System.currentTimeMillis();
                calculateTop();
                logger.info("Calculated spawners top in " + (System.currentTimeMillis() - ms) + "ms");
            }
        }.runTaskTimerAsynchronously(HeavenlySpawners.getInstance(), 0L, 20L * 60L * 5L);

        new BukkitRunnable() {
            @Override
            public void run() {
                saveAsync();
            }
        }.runTaskTimer(HeavenlySpawners.getInstance(), 0L, 20 * 30);
    }

    @Nullable
    public Spawner getSpawnerByLocation(StringLocation location) {
        return getSpawnerCache().get(location.toStringBlock());
    }

    public void calculateTop() {
        HashMap<String, Double> playerSpawnerValue = new HashMap<>();
        for (Spawner spawner : new ArrayList<>(getSpawnerCache().values())) {
            double value = 0;
            for (SpawnerInventory.SpawnerPrice spawnerPrice : SpawnerInventory.spawnerPrices) {
                if (spawnerPrice.getType().equals(spawner.getType())) {
                    value = spawnerPrice.getPrice();
                    break;
                }
            }
            value *= spawner.getStackedAmount();
            playerSpawnerValue.put(spawner.getOwner(), playerSpawnerValue.getOrDefault(spawner.getOwner(), 0D) + value);
        }
        List<String> topPlayers = playerSpawnerValue.keySet().stream().sorted(Comparator.comparing(playerSpawnerValue::get).reversed()).limit(10).collect(Collectors.toList());;
        spawnersTop.clear();
        for (String topPlayer : topPlayers) {
            spawnersTop.put(topPlayer, playerSpawnerValue.get(topPlayer));
        }
    }

    public Map<String, Spawner> getSpawnerCache() {
        return spawnerCache;
    }

    public void saveAsync() {
        if (!saving) {
            saveTask = new BukkitRunnable() {
                @Override
                public void run() {
                    save();
                    saving = false;
                }
            }.runTaskAsynchronously(HeavenlySpawners.getInstance());
        }
    }

    public int save(boolean disable) {
        if (disable && saveTask != null) saveTask.cancel();
        return save();
    }

    public int save() {
        int i = 0;
        for (Spawner spawner : new ArrayList<>(deletedSpawnerCache)) {
            i++;
            HeavenlySpawners.getInstance().getConnectionManager().deleteSpawner(spawner);
            deletedSpawnerCache.remove(spawner);
        }
        for (Spawner spawner : spawnerCache.values()) {
            if (spawner.isDirty()) {
                i++;
                HeavenlySpawners.getInstance().getConnectionManager().saveSpawner(spawner);
                spawner.setDirty(false);
            }
        }
        return i;
    }

    public List<Spawner> getDeletedSpawnerCache() {
        return deletedSpawnerCache;
    }

    public Logger getLogger() {
        return logger;
    }

    public LinkedHashMap <String, Double> getSpawnersTop() {
        return spawnersTop;
    }
}
