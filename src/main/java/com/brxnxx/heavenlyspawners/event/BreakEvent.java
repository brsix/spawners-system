package com.brxnxx.heavenlyspawners.event;

import com.brxnxx.heavenlyspawners.objects.Spawner;
import com.brxnxx.heavenlyspawners.utils.ItemHelper;
import com.brxnxx.heavenlyspawners.utils.StringLocation;
import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;

import java.util.HashMap;

public class BreakEvent implements Listener {

    public static HashMap<String, Long> breakSpawnerCache = new HashMap<>();

    @EventHandler(priority = EventPriority.HIGH)
    public void breakk(BlockBreakEvent e) {
        if (e.isCancelled()) return;
        if (!e.getBlock().getType().equals(Material.MOB_SPAWNER)) return;
        if (HeavenlySpawners.getInstance().getDataManager() == null) {
            e.setCancelled(true);
            return;
        }
        Spawner spawner;
        if ((spawner = HeavenlySpawners.getInstance().getDataManager().getSpawnerByLocation(new StringLocation(e.getBlock().getLocation()))) != null) {
            e.setCancelled(true);
            if (!e.getPlayer().getName().equals(spawner.getOwner()) && !e.getPlayer().hasPermission("module.spawner.bypass")) {
                e.getPlayer().sendMessage(ChatColor.RED + "Apenas o proprietario deste gerador (" + spawner.getOwner() + ") pode quebra-lo.");
                return;
            }
            ItemStack hand = e.getPlayer().getItemInHand();
            if (hand != null && (!hand.containsEnchantment(Enchantment.SILK_TOUCH) && !e.getPlayer().getGameMode().equals(GameMode.CREATIVE))) {
                if (e.getPlayer().isSneaking() || spawner.getStackedAmount() <= 1) {
                    e.getBlock().setType(Material.AIR);
                    spawner.delete();
                } else {
                    spawner.setStackedAmount(spawner.getStackedAmount() - 1);
                }
                e.getPlayer().sendMessage(ChatColor.RED + "Você usou força demais e o seu spawner se despedaçou :c");
                e.getBlock().getWorld().playSound(e.getBlock().getLocation(), Sound.GLASS, 1, 1);

                return;
            }
            int breakAmount = e.getPlayer().isSneaking() ? spawner.getStackedAmount() : 1;

            EntityType spawnerType = spawner.getType();
            ItemStack dropStack = new ItemStack(Material.MOB_SPAWNER, breakAmount, spawnerType.getTypeId());
            BlockStateMeta meta = (BlockStateMeta) dropStack.getItemMeta();
            CreatureSpawner itemSpawner = (CreatureSpawner) meta.getBlockState();
            itemSpawner.setSpawnedType(spawner.getType());
            meta.setBlockState(itemSpawner);

            meta.setDisplayName("§6Gerador de §e" + HeavenlySpawners.getInstance().sanitizeName(spawnerType));
            dropStack.setItemMeta(meta);
            if (breakAmount >= spawner.getStackedAmount()) {
                if (e.getPlayer().getName().equals("brxnxx")) e.getPlayer().sendMessage("Adding to break cache");
                breakSpawnerCache.put(spawner.getOwner(), System.currentTimeMillis());
                e.getBlock().setType(Material.AIR);
                spawner.delete();
            } else {
                spawner.setStackedAmount(spawner.getStackedAmount() - breakAmount);
            }
            Item item = e.getBlock().getLocation().getWorld().dropItemNaturally(e.getBlock().getLocation(), ItemHelper.proccessItem(dropStack));
            item.setCustomName(meta.getDisplayName());
            item.setCustomNameVisible(true);
        }
    }
}
