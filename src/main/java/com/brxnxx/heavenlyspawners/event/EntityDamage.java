package com.brxnxx.heavenlyspawners.event;

import com.brxnxx.heavenlyspawners.utils.EntityHelper;
import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class EntityDamage implements Listener {

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (event.getEntity() instanceof LivingEntity) {
            if (event.getEntity().getCustomName() == null) return;
            if (EntityHelper.getStackedValue((LivingEntity) event.getEntity()) > 0) {
                if (event.getEntity().getCustomName().startsWith(ChatColor.AQUA + "")) {
                    event.getEntity().remove();
                    event.setCancelled(true);
                }
            }
        }
    }
}
