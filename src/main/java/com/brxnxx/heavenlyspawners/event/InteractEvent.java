package com.brxnxx.heavenlyspawners.event;

import com.brxnxx.heavenlyspawners.inventories.SpawnerInventoryHolder;
import com.brxnxx.heavenlyspawners.objects.Spawner;
import com.brxnxx.heavenlyspawners.utils.StringLocation;
import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

public class InteractEvent implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void interact(PlayerInteractEvent e) {
        if (e.isCancelled()) return;
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getClickedBlock().getType().equals(Material.MOB_SPAWNER)) {
            if (HeavenlySpawners.getInstance().getDataManager() == null) {
                e.setCancelled(true);
                return;
            }
            Block clickedBlock = e.getClickedBlock();
            Spawner spawner = HeavenlySpawners.getInstance().getDataManager().getSpawnerByLocation(new StringLocation(clickedBlock.getLocation()));
            if (spawner != null) {
                Inventory spawnerGui = SpawnerInventoryHolder.get(spawner).getInventory().open(e.getPlayer());
                e.getPlayer().openInventory(spawnerGui);
                // OPEN GUI
            } else {
                e.getPlayer().sendMessage(ChatColor.RED + "Esse spawner é antigo, quebre-o e o coloque de volta para que volte a funcionar.");
            }
        }
    }

}
