package com.brxnxx.heavenlyspawners.event;



import com.brxnxx.heavenlyspawners.objects.Spawner;
import com.brxnxx.heavenlyspawners.utils.ItemHelper;
import com.brxnxx.heavenlyspawners.utils.StringLocation;
import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import com.brxnxx.heavenlyspawners.utils.ActionbarUtil;
import de.tr7zw.changeme.nbtapi.NBTTileEntity;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;

public class PlaceEvent implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void place(BlockPlaceEvent e) {
        if (e.isCancelled()) return;
        ItemStack itemInHand = e.getPlayer().getItemInHand();
        if (itemInHand != null && itemInHand.hasItemMeta()) {
            if (Material.MOB_SPAWNER.equals(itemInHand.getType())) {
                if (HeavenlySpawners.getInstance().getDataManager() == null) {
                    e.setCancelled(true);
                    return;
                }
                if (!itemInHand.hasItemMeta() || !itemInHand.getItemMeta().hasDisplayName()) {
                    e.getPlayer().sendMessage(ChatColor.RED + "Esse spawner é inválido.");
                    e.setCancelled(true);
                    return;
                } else if (itemInHand.hasItemMeta() && itemInHand.getItemMeta().hasDisplayName() && !itemInHand.getItemMeta().getDisplayName().startsWith(ChatColor.GOLD + "")) {
                    e.getPlayer().sendMessage(ChatColor.RED + "Esse spawner é antigo e não pode ser utilizado.");
                    e.setCancelled(true);
                    return;
                }

                CreatureSpawner itemSpawner = (CreatureSpawner) ((BlockStateMeta) itemInHand.getItemMeta()).getBlockState();
                int itemAmount = e.getPlayer().isSneaking() ? itemInHand.getAmount() : 1;

                EntityType type = itemSpawner.getSpawnedType();


                Spawner nextSpawner = getNextSpawner(e.getBlockPlaced());

                ItemStack clone = itemInHand.clone();
                clone.setAmount(itemAmount);
                int placeAmount = ItemHelper.getAmount(clone);

                if (nextSpawner == null) return;

                Spawner spawner = new Spawner(new StringLocation(e.getBlockPlaced().getLocation()), type, e.getPlayer().getName(), placeAmount);

                if (nextSpawner.getStackedAmount() == 1000000) {
                    e.getPlayer().sendMessage(ChatColor.RED + "Limite máximo de spawners atingido. (1.000.000)");
                    return;
                }

                if (spawner.getStackedAmount() + placeAmount > 1000000) {
                    e.getPlayer().sendMessage(ChatColor.RED + "Parece que se você colocar essa quantidade, irá ultrapassar o limite (" + spawner.getStackedAmount() + placeAmount + ChatColor.RED + "/1000000)");
                    return;
                }

                e.getPlayer().getInventory().removeItem(clone);

                if (nextSpawner.getOwner().equals(e.getPlayer().getName()) && nextSpawner.getType().equals(type)) {

                    e.setCancelled(true);
                    nextSpawner.setStackedAmount(nextSpawner.getStackedAmount() + placeAmount);
                    ActionbarUtil.sendActionBar(ChatColor.GREEN + "Adicionado " + (placeAmount) + " spawners ao stack.", e.getPlayer());
                } else {
                    e.getBlockPlaced().setType(Material.MOB_SPAWNER);
                    spawner.setStackedAmount(spawner.getStackedAmount());
                    HeavenlySpawners.getInstance().getDataManager().getSpawnerCache().put(new StringLocation(e.getBlockPlaced().getLocation()).toStringBlock(), spawner);

                    CreatureSpawner cspawner = (CreatureSpawner) e.getBlockPlaced().getState();

                    cspawner.setSpawnedType(type);
                    NBTTileEntity tent = new NBTTileEntity(cspawner);
                    tent.setInteger("RequiredPlayerRange", 32);
                    cspawner.update();
                }
            }
        }
    }

    private Spawner getNextSpawner(Block block) {
        for (BlockFace blockFace : BlockFace.values()) {
            if (blockFace != BlockFace.SELF) {
                Block nextTo = block.getRelative(blockFace);
                if (nextTo != null && nextTo.getType().equals(Material.MOB_SPAWNER)) {
                    Spawner spawner = HeavenlySpawners.getInstance().getDataManager().getSpawnerByLocation(new StringLocation(nextTo.getLocation()));
                    if (spawner != null) return spawner;
                }
            }
        }
        return null;
    }

}
