package com.brxnxx.heavenlyspawners.event;

import com.brxnxx.heavenlyspawners.objects.Spawner;
import com.brxnxx.heavenlyspawners.translator.EntityName;
import com.brxnxx.heavenlyspawners.utils.StringLocation;
import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import com.brxnxx.heavenlyspawners.utils.EntityHelper;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;

public class SpawnerEvent implements Listener {

    @EventHandler
    public void onSpawnerSpawnEvent(SpawnerSpawnEvent e) {
        if (HeavenlySpawners.getInstance().getDataManager() == null) {
            e.getEntity().remove();
            e.setCancelled(true);
            return;
        }


        Spawner spawner = HeavenlySpawners.getInstance().getDataManager().getSpawnerByLocation(new StringLocation(e.getSpawner().getLocation()));
        if (spawner != null) {

            if (BreakEvent.breakSpawnerCache.containsKey(spawner.getOwner())) {
                if (BreakEvent.breakSpawnerCache.get(spawner.getOwner()) > (System.currentTimeMillis() - 7000)) {
                    e.getEntity().remove();
                    e.setCancelled(true);
                    return;
                } else {
                    BreakEvent.breakSpawnerCache.remove(spawner.getOwner());
                }
            }

            LivingEntity toStack = null;
            LivingEntity spawnedEntity = (LivingEntity) e.getEntity();

            int stacked = 0;
            for (LivingEntity entity : EntityHelper.getEntities(spawnedEntity.getLocation(), 20.0, spawnedEntity.getType())) {
                if (spawnedEntity == entity || !entity.isCustomNameVisible() || entity.getCustomName().startsWith(ChatColor.AQUA + "")) continue;
                stacked = EntityHelper.getStackedValue(entity);
                if (stacked <= 0) {
                    entity.remove();
                    continue;
                }
                toStack = entity;
                break;
            }

            if (toStack != null) {
               e.getEntity().remove();
               stacked += spawner.getStackedAmount();
            } else {
                toStack = spawnedEntity;
                stacked = spawner.getStackedAmount();
            }

            spawnedEntity.getLocation().getWorld().playEffect(spawnedEntity.getLocation(), Effect.MOBSPAWNER_FLAMES, 250, 250);
            toStack.setCustomName(EntityHelper.getEntityNameFormat(EntityName.byEntity(toStack.getType()), stacked));
            if (toStack.isCustomNameVisible()) return;
            toStack.setCustomNameVisible(true);
        } else {
            e.getEntity().remove();
            e.setCancelled(true);
        }
    }

    @EventHandler(priority= EventPriority.HIGH)
    public void on(CreatureSpawnEvent e) {
        if (e.getSpawnReason() != CreatureSpawnEvent.SpawnReason.SPAWNER) {
            e.setCancelled(true);
        }
    }
}
