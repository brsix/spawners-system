package com.brxnxx.heavenlyspawners.inventories;

import com.brxnxx.heavenlyspawners.utils.gui.ChatGUI;
import com.brxnxx.heavenlyspawners.utils.gui.ClickableItem;
import com.brxnxx.heavenlyspawners.utils.gui.SmartInventory;
import com.brxnxx.heavenlyspawners.utils.gui.content.InventoryContents;
import com.brxnxx.heavenlyspawners.utils.gui.content.InventoryProvider;
import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import com.brxnxx.heavenlyspawners.utils.HumanUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

import static com.brxnxx.heavenlyspawners.utils.ItemHelper.proccessItem;

public class SpawnerInventory {
    private static SpawnerInventory instance;
    public static List<SpawnerPrice> spawnerPrices = Arrays.asList(
            new SpawnerPrice(EntityType.COW, 750000D),
            new SpawnerPrice(EntityType.ZOMBIE, 11750000D),
            new SpawnerPrice(EntityType.SKELETON, 230975000D),
            new SpawnerPrice(EntityType.WITCH, 4320200000D),
            new SpawnerPrice(EntityType.MUSHROOM_COW, 61230000000D),
            new SpawnerPrice(EntityType.BLAZE, 1234262880000D),
            new SpawnerPrice(EntityType.IRON_GOLEM, 15897600000000D),
            new SpawnerPrice(EntityType.VILLAGER, 310210560000000D));

    private SmartInventory inventory;

    public static SpawnerInventory get() {
        if (instance == null) instance = new SpawnerInventory();
        return instance;
    }

    public SmartInventory getInventory() {
        return inventory;
    }

    private SpawnerInventory() {
        inventory = SmartInventory.builder()
                .provider(new SpawnerInventoryProvider())
                .size(4, 9)
                .title("Loja de Spawners")
                .closeable(true)
                .manager(HeavenlySpawners.getInvManager())
                .build();
    }

    private void doPurchase(Player player, SpawnerPrice spawner, int amount) {
        double price = spawner.getPrice() * amount;
        player.sendMessage(ChatColor.GOLD + "Finalizando a compra de " + ChatColor.BOLD + amount + " itens" + ChatColor.GOLD + " por " + ChatColor.YELLOW + HumanUtils.formatNumber(price) + ChatColor.GOLD + "...");
        if (!HeavenlySpawners.getInstance().getEconomy().has(player, price)) {
            player.sendMessage(ChatColor.RED + "Você não tem dinheiro suficiente!");
            return;
        }
        HeavenlySpawners.getInstance().getEconomy().withdrawPlayer(player, price);

        ItemStack dropStack = new ItemStack(Material.MOB_SPAWNER, amount, spawner.getType().getTypeId());
        BlockStateMeta meta = (BlockStateMeta) dropStack.getItemMeta();
        CreatureSpawner itemSpawner = (CreatureSpawner) meta.getBlockState();
        itemSpawner.setSpawnedType(spawner.getType());
        meta.setBlockState(itemSpawner);
        meta.setDisplayName(ChatColor.GOLD + "Gerador de " + ChatColor.YELLOW + HeavenlySpawners.getInstance().sanitizeName(spawner.getType()));
        dropStack.setItemMeta(meta);
        player.getInventory().addItem(proccessItem(dropStack));

        player.sendMessage(ChatColor.GOLD + "Você comprou " + ChatColor.BOLD + amount + " " + ChatColor.stripColor(dropStack.getItemMeta().getDisplayName()) + ChatColor.GOLD + " por " + ChatColor.YELLOW + HumanUtils.formatNumber(price) + ChatColor.GOLD + ".");
    }

    public static class SpawnerPrice {
        private EntityType type;
        private double price;

        public SpawnerPrice(EntityType type, double price) {
            this.type = type;
            this.price = price;
        }

        public EntityType getType() {
            return type;
        }

        public double getPrice() {
            return price;
        }
    }

    private class SpawnerInventoryProvider implements InventoryProvider {

        @Override
        public void init(Player player, InventoryContents contents) {
            int column = 2;
            int row = 1;
            for (SpawnerPrice spawnerPrice : spawnerPrices) {
                ItemStack spawner = new ItemStack(Material.MOB_SPAWNER);
                ItemMeta meta = spawner.getItemMeta();
                meta.setDisplayName(ChatColor.GOLD + "Gerador de " + ChatColor.YELLOW + HeavenlySpawners.getInstance().sanitizeName(spawnerPrice.getType()));
                meta.setLore(Arrays.asList("", ChatColor.GREEN + "Valor por unidade: " + ChatColor.WHITE + "" + HumanUtils.humanBalance(spawnerPrice.getPrice()), ""));
                spawner.setItemMeta(meta);
                contents.set(row, column, ClickableItem.of(spawner, inventoryClickEvent -> {
                    player.closeInventory();
                    new ChatGUI(player, "\n\n" + ChatColor.GOLD + "   Digite a quantidade de itens que deseja comprar." + ChatColor.GOLD + "\n   Cancele a compra digitando " + ChatColor.YELLOW + "\"cancelar\"" + ChatColor.GOLD + " no chat." + ChatColor.YELLOW +  "\n\n    ").init(HeavenlySpawners.getInstance(), chatGUI -> {
                        if (chatGUI.getLastResponse().equalsIgnoreCase("cancelar")) return true;
                        int amount;
                        try {
                            String response = chatGUI.getLastResponse();
                            amount = Integer.parseInt(response);
                        } catch (NumberFormatException e) {
                            player.sendMessage(ChatColor.RED + chatGUI.getLastResponse() + ChatColor.RED + " não é um número válido, tenta novamente ou digite \"cancelar\" para cancelar.");
                            return false;
                        }

                        if (amount > 500000) {
                            player.sendMessage(ChatColor.RED + String.valueOf(amount) + ChatColor.RED + " ultrapassa o limite de compras (500k)");
                            return false;
                        }

                        doPurchase(player, spawnerPrice, amount);
                        return true;
                    });
                }));
                column++;
                if (column > 6) {
                    column = 2;
                    row = 2;
                }
            }
        }

        @Override
        public void update(Player player, InventoryContents contents) {

        }
    }
}
