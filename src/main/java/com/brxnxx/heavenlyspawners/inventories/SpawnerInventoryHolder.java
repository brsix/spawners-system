package com.brxnxx.heavenlyspawners.inventories;

import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import com.brxnxx.heavenlyspawners.objects.Spawner;
import com.brxnxx.heavenlyspawners.translator.EntityName;
import com.brxnxx.heavenlyspawners.utils.gui.ClickableItem;
import com.brxnxx.heavenlyspawners.utils.gui.SmartInventory;
import com.brxnxx.heavenlyspawners.utils.gui.content.InventoryContents;
import com.brxnxx.heavenlyspawners.utils.gui.content.InventoryProvider;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class SpawnerInventoryHolder {

    private static SpawnerInventoryHolder instance;

    private SmartInventory inventory;

    public SmartInventory getInventory() { return inventory; }

    public static SpawnerInventoryHolder get(Spawner spawner) {
        if (instance == null) instance = new SpawnerInventoryHolder(spawner);
        return instance;
    }

    public SpawnerInventoryHolder(Spawner spawner) {
        inventory = SmartInventory.builder()
                .provider(new SpawnerInventoryHolderProvider(spawner))
                .size(3, 9)
                .title("Gerador de " + EntityName.byEntity(spawner.getType()))
                .closeable(true)
                .manager(HeavenlySpawners.getInvManager())
                .build();
    }

    public class SpawnerInventoryHolderProvider implements InventoryProvider {

        private Spawner spawner;

        public SpawnerInventoryHolderProvider(Spawner spawner) { this.spawner = spawner; }

        private ItemStack getGerador() {
            ItemStack gerador = new ItemStack(Material.MOB_SPAWNER, 1);
            ItemMeta meta = gerador.getItemMeta();
            meta.setDisplayName("§6Informações do spawner");
            meta.setLore(Arrays.asList("",
                    "§6Quantidade: " + spawner.getStackedAmount(),
                    "§6Dono: " + spawner.getOwner(),
                    "§6Entidade: " + EntityName.byEntity(spawner.getType()),
                    ""));
            meta.setDisplayName(ChatColor.GREEN + "" + spawner.getStackedAmount() + " geradore(s) de " + ChatColor.BOLD + EntityName.byEntity(spawner.getType()));
            gerador.setItemMeta(meta);

            return gerador;
        }

        private ItemStack getDye() {
            ItemStack dye = new ItemStack(Material.INK_SACK, 1, (spawner.isEnabled()) ? (byte) 10 : (byte) 8);
            ItemMeta dyeMeta = dye.getItemMeta();
            dyeMeta.setDisplayName("§6Ativar/desativar spawner");
            dyeMeta.setLore(Arrays.asList("",
                    "",
                    "§6Clique para " + (spawner.isEnabled() ? "§aativar" : "§cdesativar"),
                    ""
            ));

            return dye;
        }

        private ItemStack getBarrier() {
            ItemStack barrier = new ItemStack(Material.BARRIER);
            ItemMeta barrierMeta = barrier.getItemMeta();
            barrierMeta.setDisplayName("§cRemover todos os spawners");
            barrierMeta.setLore(Arrays.asList("", "§cClique para remover todos", "§cos spawners. Nota:", "§cse seu inventário estiver", "§ccheio, eles serão dropados.", ""));
            barrier.setItemMeta(barrierMeta);

            return barrier;
        }

        public Spawner getSpawner() {
            return spawner;
        }

        @Override
        public void init(Player player, InventoryContents contents) {
            contents.set(1, 1, ClickableItem.of(getGerador(), inventoryClickEvent -> {
                player.sendMessage("oi kk " + spawner.getStackedAmount());
            }));

            contents.set(2, 2, ClickableItem.of(new ItemStack(Material.GRASS), inventoryClickEvent -> {
                inventoryClickEvent.getWhoClicked().sendMessage("op");
            }));
        }

        @Override
        public void update(Player player, InventoryContents contents) {

        }
    }

}