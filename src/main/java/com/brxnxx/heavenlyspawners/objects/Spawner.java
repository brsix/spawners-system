package com.brxnxx.heavenlyspawners.objects;



import com.brxnxx.heavenlyspawners.utils.StringLocation;
import com.brxnxx.heavenlyspawners.HeavenlySpawners;
import org.bukkit.entity.EntityType;

import java.util.Objects;

public class Spawner {
    private StringLocation location;
    private EntityType type;
    private String owner;
    private int stackedAmount;
    private boolean dirty;
    private boolean enabled;

    public Spawner(StringLocation location, EntityType type, String owner, int stackedAmount) {
        this.location = location;
        this.type = type;
        this.owner = owner;
        this.stackedAmount = stackedAmount;
        this.enabled = false;
    }

    public boolean isEnabled() { return enabled; }

    public void disable() { this.enabled = false; }

    public StringLocation getLocation() {
        return location;
    }

    public EntityType getType() {
        return type;
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
        setDirty(true);
    }

    public int getStackedAmount() {
        return stackedAmount;
    }

    public void setStackedAmount(int stackedAmount) {
        this.stackedAmount = stackedAmount;
        setDirty(true);
    }

    public void delete() {
        HeavenlySpawners.getInstance().getDataManager().getDeletedSpawnerCache().add(this);
        HeavenlySpawners.getInstance().getDataManager().getSpawnerCache().remove(location.toStringBlock());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Spawner spawner = (Spawner) o;
        return stackedAmount == spawner.stackedAmount &&
                Objects.equals(location, spawner.location) &&
                type == spawner.type &&
                Objects.equals(owner, spawner.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location);
    }
}
