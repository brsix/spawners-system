package com.brxnxx.heavenlyspawners.translator;

import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.EntityType;

public enum EntityName {
    AREA_EFFECT_CLOUD("\u00c1rea de Efeito de Po\u00e7\u00e3o"),
    ARMOR_STAND("Suporte para Armaduras"),
    ARROW("Flecha"),
    BAT("Morcego"),
    BLAZE("Blaze"),
    BOAT("Barco"),
    CAVE_SPIDER("Aranha da Caverna"),
    CHICKEN("Galinha"),
    COD("Bacalhau"),
    COMPLEX_PART("Desconhecido"),
    COW("Vaca"),
    CREEPER("Creeper"),
    DOLPHIN("Golfinho"),
    DONKEY("Burro"),
    DRAGON_FIREBALL("Bola de Fogo"),
    DROPPED_ITEM("Item dropado"),
    DROWNED("Afogado"),
    EGG("Ovo"),
    ELDER_GUARDIAN("Guardi\u00e3o Mestre"),
    ENDER_CRYSTAL("Cristal do End"),
    ENDER_DRAGON("Drag\u00e3o do Fim"),
    ENDER_PEARL("P\u00e9rola do Fim"),
    ENDER_SIGNAL("Olho do Fim"),
    ENDERMAN("Enderman"),
    ENDERMITE("Endermite"),
    EVOKER("Invocador"),
    EVOKER_FANGS("Presas do Invocador"),
    EXPERIENCE_ORB("Orb de Experi\u00eancia"),
    FALLING_BLOCK("Bloco"),
    FIREBALL("Bola de Fogo"),
    FIREWORK("Fogos de Artif\u00edcio"),
    FISHING_HOOK("Isca de Pesca"),
    GHAST("Ghast"),
    GIANT("Zumbi Gigante"),
    GUARDIAN("Guardi\u00e3o"),
    HORSE("Cavalo"),
    HUSK("Zumbi do Deserto"),
    ILLUSIONER("Ilusionista"),
    IRON_GOLEM("Golem de Ferro"),
    ITEM_FRAME("Moldura"),
    LEASH_HITCH("Desconhecido"),
    LIGHTNING("Raio"),
    LINGERING_POTION("Po\u00e7\u00e3o"),
    LLAMA("Lhama"),
    LLAMA_SPIT("Cuspe de Lhama"),
    MAGMA_CUBE("Cubo de Magma"),
    MINECART("Carrinho"),
    MINECART_CHEST("Carrinho com Ba\u00fa"),
    MINECART_COMMAND("Carrinho com Bloco de Comando"),
    MINECART_FURNACE("Carrinho com Fornalha"),
    MINECART_HOPPER("Carrinho com Funil"),
    MINECART_MOB_SPAWNER("Carrinho com Gerador de Monstros"),
    MINECART_TNT("Carrinho com Dinamite"),
    MULE("Mula"),
    MUSHROOM_COW("Vaca de Cogumelo"),
    OCELOT("Jaguatirica"),
    PAINTING("Pintura"),
    PARROT("Papagaio"),
    PHANTOM("Phantom"),
    PIG("Porco"),
    PIG_ZOMBIE("Porco Zumbi"),
    PLAYER("Player"),
    POLAR_BEAR("Urso Polar"),
    PRIMED_TNT("Dinamite"),
    PUFFERFISH("Baiacu"),
    RABBIT("Coelho"),
    SALMON("Salm\u00e3o"),
    SHEEP("Ovelha"),
    SHULKER("Shulker"),
    SHULKER_BULLET("Dardo de Shulker"),
    SILVERFISH("Silverfish"),
    SKELETON("Esqueleto"),
    SKELETON_HORSE("Cavalo Esqueleto"),
    SLIME("Slime"),
    SMALL_FIREBALL("Bola de Fogo Pequena"),
    SNOWBALL("Bola de Neve"),
    SNOWMAN("Boneco de Neve"),
    SPECTRAL_ARROW("Flecha Espectral"),
    SPIDER("Aranha"),
    SPLASH_POTION("Po\u00e7\u00e3o Arremess\u00e1vel"),
    SQUID("Lula"),
    STRAY("Esqueleto Vagante"),
    THROWN_EXP_BOTTLE("Frasco de Experi\u00eancia"),
    TIPPED_ARROW("Flecha"),
    TRIDENT("Tridente"),
    TROPICAL_FISH("Peixe Tropical"),
    TURTLE("Tartaruga"),
    UNKNOWN("Desconhecido"),
    VEX("Fantasma"),
    VILLAGER("Vilager"),
    VINDICATOR("Vingador"),
    WEATHER("Chuva"),
    WITCH("Bruxa"),
    WITHER("Wither"),
    WITHER_SKELETON("Esqueleto Wither"),
    WITHER_SKULL("Cabe\u00e7a do Wither"),
    WOLF("Lobo"),
    ZOMBIE("Zumbi"),
    ZOMBIE_HORSE("Cavalo Zumbi"),
    ZOMBIE_VILLAGER("Alde\u00e3o Zumbi");

    private final String translation;

    private EntityName(String name) {
        this.translation = name;
    }

    public EntityType getType() {
        return EntityType.valueOf(this.name());
    }

    @Nullable
    public static EntityName byName(String name) {
        for (EntityName value : EntityName.values()) {
            if (value.translation.equalsIgnoreCase(name)) {
                return value;
            }
        }
        return null;
    }

    public static String byEntity(EntityType entityType) {
        try {
            return EntityName.valueOf((String)entityType.name()).translation;
        }
        catch (Throwable e) {
            try {
                return EntityName.valueOf((String)entityType.getName()).translation;
            }
            catch (Throwable e1) {
                return entityType.toString();
            }
        }
    }
}

