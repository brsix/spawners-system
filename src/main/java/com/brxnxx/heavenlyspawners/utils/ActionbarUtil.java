package com.brxnxx.heavenlyspawners.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ActionbarUtil {
    private static final String BV = Bukkit.getServer().getClass().getPackage().getName().substring(23);
    private static boolean initialised = false;
    private static Constructor<?> chatSer;
    private static Constructor<?> packetChat;
    private static Method getPlayerHandle;
    private static Field playerConnection;
    private static Method sendPacket;

    static {
        try {
            chatSer = Class.forName("net.minecraft.server." + BV + ".ChatComponentText").getConstructor(String.class);
            packetChat = Class.forName("net.minecraft.server." + BV + ".PacketPlayOutChat").getConstructor(Class.forName("net.minecraft.server." + BV + ".IChatBaseComponent"), byte.class);
            getPlayerHandle = Class.forName("org.bukkit.craftbukkit." + BV + ".entity.CraftPlayer").getDeclaredMethod("getHandle");
            playerConnection = Class.forName("net.minecraft.server." + BV + ".EntityPlayer").getDeclaredField("playerConnection");
            sendPacket = Class.forName("net.minecraft.server." + BV + ".PlayerConnection").getDeclaredMethod("sendPacket", Class.forName("net.minecraft.server." + BV + ".Packet"));
            initialised = true;
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            initialised = false;
        }
    }

    public static boolean isInitialised(){
        return initialised;
    }

    public static boolean sendActionBar(String message, Player... p) {
        if (!initialised) return false;
        try {
            Object o = chatSer.newInstance(message);
            Object packet = packetChat.newInstance(o, (byte) 2);
            sendPacket(packet, p);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            initialised = false;
        }
        return initialised;
    }

    private static void sendPacket(Object packet, Player... pl)
            throws ReflectiveOperationException {
        for (Player p : pl) {
            Object entityplayer = getPlayerHandle.invoke(p);
            Object PlayerConnection = playerConnection.get(entityplayer);
            sendPacket.invoke(PlayerConnection, packet);
        }
    }
}
