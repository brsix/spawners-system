package com.brxnxx.heavenlyspawners.utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EntityHelper {
    private static final Pattern NAME_PATTERN = Pattern.compile("(?<!\\.)\\d+(?!\\.)");
    private static final String NAME_FORMAT = ChatColor.GREEN + "%s %dx";

    public static String getEntityNameFormat(String name, int stacked) {
        return String.format(NAME_FORMAT, name, stacked);
    }

    public static int getStackedValue(LivingEntity entity) {
        Matcher matcher = NAME_PATTERN.matcher(entity.getCustomName());
        if (!matcher.find()) { return -1; }
        try { return Integer.parseInt(matcher.group()); }
        catch (NumberFormatException e) { return -1; }
    }

    public static LivingEntity getFirstEntity(Location location, double radius) {
        for (Entity nearbyEntity : location.getWorld().getNearbyEntities(location, radius, radius, radius)) {
            if (nearbyEntity instanceof LivingEntity && !nearbyEntity.isDead()) {
                return (LivingEntity) nearbyEntity;
            }
        }
        return null;
    }

    public static List<LivingEntity> getEntities(Location location, double radius, EntityType type) {
        ArrayList<LivingEntity> entities = new ArrayList<LivingEntity>();
        Iterator iterator = location.getWorld().getNearbyEntities(location, radius, radius, radius).iterator();
        for (Entity nearbyEntity : location.getWorld().getNearbyEntities(location, radius, radius, radius)) {
            if (nearbyEntity instanceof LivingEntity && !nearbyEntity.isDead() && nearbyEntity.getType().equals(type)) {
                entities.add((LivingEntity) nearbyEntity);
            }
        }
        return entities;
    }

}
