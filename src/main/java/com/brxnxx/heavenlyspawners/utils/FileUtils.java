package com.brxnxx.heavenlyspawners.utils;

import java.io.File;
import java.io.IOException;

public class FileUtils {

    public static File loadFile(File dataFolder, String string) {
        File file = new File(dataFolder, string);

        return loadFile(file, true);
    }

    public static File loadFileWithoutCreating(File dataFolder, String string) {
        File file = new File(dataFolder, string);

        return loadFile(file, false);
    }

    public static File loadFile(File file, boolean create) {
        if (!file.exists()) {
            try {
                if (file.getParent() != null) {
                    file.getParentFile().mkdirs();
                }

                if (create) file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }


}
