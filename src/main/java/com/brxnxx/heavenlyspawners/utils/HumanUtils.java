package com.brxnxx.heavenlyspawners.utils;

import java.text.NumberFormat;
import java.util.Locale;

public class HumanUtils {

    public static String formatNumber(final double d) {
        final NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(0);
        return format.format(d);
    }

    public static String humanBalance(double balance) {
        if (balance < 1000) {
            return formatNumber(balance);
        }
        if (balance < 1000000) {
            return formatNumber(balance / 1000.0) + "K";
        }
        if (balance < 1E9) {
            return formatNumber(balance / 1000000.0) + "M";
        }
        if (balance < 1E12) {
            return formatNumber(balance / 1.0E9) + "B";
        }
        if (balance < 1E15) {
            return formatNumber(balance / 1.0E12) + "t";
        }
        if (balance < 1E18) {
            return formatNumber(balance / 1.0E15) + "q";
        }
        if (balance < 1E21) {
            return formatNumber(balance / 1.0E18) + "Q";
        }
        if (balance < 1E24) {
            return formatNumber(balance / 1.0E21) + "s";
        }
        if (balance < 1E27) {
            return formatNumber(balance / 1.0E24) + "S";
        }
        if (balance < 1E30) {
            return formatNumber(balance / 1.0E27) + "o";
        }
        if (balance < 1E33) {
            return formatNumber(balance / 1.0E30) + "n";
        }
        if (balance < 1E36) {
            return formatNumber(balance / 1.0E33) + "d";
        }
        if (balance < 1E39) {
            return formatNumber(balance / 1.0E36) + "U";
        }
        if (balance < 1E42) {
            return formatNumber(balance / 1.0E39) + "D";
        }
        if (balance < 1E45) {
            return formatNumber(balance / 1.0E42) + "T";
        }
        if (balance < 1E48) {
            return formatNumber(balance / 1.0E45) + "Qt";
        }
        if (balance < 1E51) {
            return formatNumber(balance / 1.0E48) + "Qd";
        }
        if (balance < 1E54) {
            return formatNumber(balance / 1.0E51) + "Sd";
        }
        if (balance < 1E57) {
            return formatNumber(balance / 1.0E54) + "St";
        }
        if (balance < 1E60) {
            return formatNumber(balance / 1.0E57) + "O";
        }
        if (balance < 1E63) {
            return formatNumber(balance / 1.0E60) + "N";
        }
        if (balance < 1E303) {
            return formatNumber(balance / 1.0E63) + "v";
        }
        return formatNumber(balance / 1.0E303) + "C";
    }

}
