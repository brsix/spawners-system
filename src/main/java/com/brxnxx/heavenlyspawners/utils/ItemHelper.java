package com.brxnxx.heavenlyspawners.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ItemHelper {

    public static ItemStack proccessItem(ItemStack stack) {
        if (stack.getType().equals(Material.MOB_SPAWNER) && stack.getAmount() > 1) {
            ItemMeta meta = stack.getItemMeta();
            meta.setLore(Collections.singletonList(ChatColor.YELLOW + "Quantidade: " + stack.getAmount()));
            stack.setItemMeta(meta);
            stack.setAmount(1);
        }
        return stack;
    }

    public static int getAmount(ItemStack stack) {
        ItemMeta meta = stack.getItemMeta();
        if (meta.hasLore() && meta.getLore().size() > 0) {
            if (meta.getLore().get(0).contains("Quantidade: ")) {
                Pattern p = Pattern.compile("\\d+");
                Matcher m = p.matcher(meta.getLore().get(0));
                if (m.find()) return Integer.parseInt(m.group()) * stack.getAmount();
            }
        }
        return stack.getAmount();
    }

}
