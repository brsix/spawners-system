package com.brxnxx.heavenlyspawners.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Logger {
    private String id;
    private String plugin;

    public Logger(String plugin) {
        this.plugin = plugin;
    }

    public Logger(String plugin, String id) {
        this.plugin = plugin;
        this.id = id;
    }

    public Logger getLogger(String id) {
        return new Logger(plugin, id);
    }

    public void info(String message) {
        send(message, Level.INFO);
    }

    public String getPrefix() {
        return "[" + plugin + (id == null ? "" : "-" + id) + "] ";
    }

    private void send(String message, Level level) {
        Bukkit.getConsoleSender().sendMessage(level.getColor() + getPrefix() + message);
    }

    public void debug(String message) {
        send(message, Level.DEBUG);
    }

    public void error(String message) {
        send(message, Level.ERROR);
    }

    public void warn(String message) {
        send(message, Level.WARN);
    }

    public enum Level {
        INFO(ChatColor.GREEN), WARN(ChatColor.GOLD), ERROR(ChatColor.RED), DEBUG(ChatColor.LIGHT_PURPLE);

        ChatColor color;

        Level(ChatColor color) {
            this.color = color;
        }

        public ChatColor getColor() {
            return color;
        }
    }
}
