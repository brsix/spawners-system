package com.brxnxx.heavenlyspawners.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class StringLocation extends Location {
    public StringLocation(World world, double x, double y, double z) {
        super(world, x, y, z);
    }

    public StringLocation(World world, double x, double y, double z, float yaw, float pitch) {
        super(world, x, y, z, yaw, pitch);
    }

    public StringLocation(Location l) {
        super(l.getWorld(), l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch());
    }

    public static StringLocation fromString(String s) {
        String[] split = s.split("::");
        if (split.length > 3) {
            World world = Bukkit.getWorld(split[0]);
            double x = Double.parseDouble(split[1]);
            double y = Double.parseDouble(split[2]);
            double z = Double.parseDouble(split[3]);
            if (split.length > 5) {
                float yaw = Float.parseFloat(split[4]);
                float pitch = Float.parseFloat(split[5]);
                return new StringLocation(world, x, y, z, yaw, pitch);
            }
            return new StringLocation(world, x, y, z);
        }
        return null;
    }

    public String toStringBlock() {
        return getWorld().getName() + "::" + getBlockX() + "::" + getBlockY() + "::" + getBlockZ();
    }

    public String toString() {
        return getWorld().getName() + "::" + getX() + "::" + getY() + "::" + getZ() + "::" + getYaw() + "::" + getPitch();
    }
}
