package com.brxnxx.heavenlyspawners.utils.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.function.Predicate;

public class ChatGUI implements Listener {
    private Player player;
    private String message;
    private Predicate<ChatGUI> predicate;
    private String lastResponse;

    public ChatGUI(Player player, String message) {
        this.player = player;
        this.message = ChatColor.translateAlternateColorCodes('&', message);
    }

    public void init(JavaPlugin plugin, Predicate<ChatGUI> predicate) {
        this.predicate = predicate;
        Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
        player.sendMessage(message);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (event.getPlayer().equals(player)) {
            event.setCancelled(true);
            lastResponse = event.getMessage();
            if (predicate.test(this)) {
               destroy();
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (event.getPlayer().equals(player)) {
            destroy();
        }
    }

    public void destroy() {
        predicate = null;
        HandlerList.unregisterAll(this);
    }

    public String getLastResponse() {
        return ChatColor.translateAlternateColorCodes('&', lastResponse);
    }

    public Player getPlayer() {
        return player;
    }
}
